package eu.dreamix.ms;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@EnableEurekaClient
@EnableConfigServer
@SpringBootApplication
public class MsConfigServerApplication {

	 @Value("${message}")
	    private String message;
	 
    public static void main(String[] args) {
        SpringApplication.run(MsConfigServerApplication.class, args);
    }
    
    @RequestMapping(value = "/message", 
    	      method = RequestMethod.GET, 
    	      produces = MediaType.TEXT_PLAIN_VALUE)
    	    public String hello() {
    	        return message;
    	    }
}
